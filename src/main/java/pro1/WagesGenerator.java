package pro1;

import java.math.BigDecimal;

public interface WagesGenerator
{
    int getWage(String userName);
}
