package pro1.implementations;

import pro1.WagesGenerator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

public class CommitsWagesGenerator implements WagesGenerator {

    HashMap<String, Integer> data= new HashMap<>();

    public CommitsWagesGenerator()
    {
        // Načti CSV soubor
    }

    public void load()
    {
        try {
            Path path = Paths.get(System.getProperty("user.dir"), "input", "commits.csv");
            File file = new File(path.toString());
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    break;
                }
                String[] split = line.split(",");
                if(!data.containsKey(split[0]))
                {
                    data.put(split[0], 20000);
                }
                // uz jsem se ujistil ze login je jako klic v hasmape
                int commited = Integer.parseInt(split[2]);
                int newValue;
                if(commited > 0)
                {
                    newValue = data.get(split[0]) + commited *50 ;
                }
                else
                {
                    newValue = data.get(split[0]) + (-commited) * 100 ;
                }
                data.put(split[0], newValue);
            }
            fr.close();
        }
        catch (Exception e)
        {

        }
    }

    @Override
    public int getWage(String userName) {
        return data.get(userName);
    }
}
