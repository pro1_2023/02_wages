package pro1.implementations;

import pro1.WagesGenerator;

import java.util.Random;

public class RandomWagesGenerator implements WagesGenerator
{
    @Override
    public int getWage(String userName)
    {
        Random random = new Random();
        return random.nextInt();
    }
}
