package pro1.implementations;

import pro1.WagesGenerator;

public class EqualWagesGenerator implements WagesGenerator
{
    public EqualWagesGenerator(int equalWage)
    {
        this.equalWage = equalWage;
    }

    private int equalWage;
    @Override
    public int getWage(String userName)
    {
        return equalWage;
    }
}
