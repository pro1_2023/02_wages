package pro1;

import pro1.implementations.CommitsWagesGenerator;
import pro1.implementations.EqualWagesGenerator;
import pro1.implementations.RandomWagesGenerator;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;


public class Main
{
    public static void main(String[] args) throws IOException
    {
        CommitsWagesGenerator wagesGenerator =new CommitsWagesGenerator();
        wagesGenerator.load();
        Scanner scanner = new Scanner(System.in);
        while (true)
        {
            String userName = scanner.nextLine();
            int wage = wagesGenerator.getWage(userName);
            System.out.println("Mzda pro zamestanace " + userName + " je: " + wage + " Kc");
        }
    }

    public static void readCommitsFile() throws IOException
    {
        Path path = Paths.get(System.getProperty("user.dir"),"input","commits.csv");
        File file = new File(path.toString());
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        while(true)
        {
            String line = br.readLine();
            if(line == null)
            {
                break;
            }
            System.out.println(line);
        }
        fr.close();
    }
}

